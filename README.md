# Mustached SandWorm

## Intro
A colleague has a client who frequently gives her new images to add to his static HTML site. For years, the team laboriously updated the HTML files by hand. That is, until the Mustached SandWorm arose and showed us a better way.

From deep within the earth, he brought us a precious Ruby whose gems could set us free from our sorrows. Now we no longer fear HTML updates, knowing that the power of the Ruby is only a command line away.

## Usage
_NOTE: The Ruby is happiest and easiest to use when run from your site's root directory_

_NOTE: Usage is fairly constrained to a specific HTML page at this point. Please see below for a sample of the HTML this script expects to inspect. More flexibility may be added later on._

Start the script via the command line and you'll be prompted for several pieces of information:

* The directory where the new images are stored. Should be in MM.YYYY format, i.e., 08.2014.

* The HTML file to be updated. Note that the script will keep processing HTML files until all of the new images have been added.

* Image title prefix. In the HTML page that gets displayed, it's assumed that you want your images to have a title.

* Starting point for image numbering. It's assumed that, in addion to having a title, you want your images to be numbered.

## Versions

### 0.2.0
* Fixed a bug where too many or too few slashes would end up being written to HTML files

### 0.1.0
* Basic functionality for updating HTML files
* Does some basic error checking to ensure that it receives valid arguments

## Example HTML Page

    <div id="thumb">
        <a href="javascript:;"onclick="MM_openBrWindow('../images/web_large/wondrous_places/vermont./08.2014/1004.jpg','','width=850,height=543');return false">
            <img src="../images/thumbs/wondrous_places/vermont./08.2014/1004.jpg" width="150" height="100" border="0" />
        </a>
        <p>test0014</p>
    </div>