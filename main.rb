# ----------------
# Include the necessary stuff
# ----------------
require 'dimensions' # Used to determine images dimensions and inject them into HTML
require 'htmlentities' # Used to read and write HTML special characters (think PHP htmlentities)

# ----------------
# Class: ImageManager
# Description: Class that keeps track of all FileTextUpdater to be updated
# ----------------
class ImageManager

	@image_directory
	@image_array
	@image_array_init_len
    
	# 
	# On initialization, get the directory where the new images live and, for each image, add the image and its dimensions to an array and add that array to an array of all images
	#
	def initialize(image_directory)
		@image_directory = image_directory # full path to the images
		@image_array = Array.new()

		Dir.entries(@image_directory).each do |filename|
		        if filename =~ /\w+\.(gif|jpg|jpeg|png|tif|tiff)/ # We only want image files
        			@image_array.push(filename)
		        	#puts filename
	        	end
		end
        
        	@image_array.sort_by!{ |name| name } # Put the images in alphabetical order (actually, they end up backwards from what you'd expect when using array.pop, so...)
        	@image_array.reverse! # Reverse the images to the expected order
	        @image_array_init_len = @image_array.length
        
		#@image_array.each do |image|
	        	#puts image
	        #end
	end

	#
	# Returns the next image in our image array
	#
	def nextImage()
		return @image_array.pop
	end
    
	def getNumImages()
		return @image_array_init_len
	end

	#
	# Returns the parent folders for our images
	#
	def getFolderWhereImagesLive()
		return @image_directory
	end

end

# ----------------
# Class: FileTextUpdater
# Description: Opens a file and updates its HTML with the new image names and dimensions + the user's specified prefix and image numbering
# ----------------
class FileTextUpdater

	@file_path # Path to the HTML file to be edited
	@this_replacement_images # Object reference for the images to be updated
	@image_prefix
	@image_numbering
	@counter

	def initialize(file_path, this_replacement_images, image_prefix, image_numbering)
		@file_path = file_path
		@this_replacement_images = this_replacement_images
		@image_prefix = image_prefix
		@image_numbering = image_numbering
		@counter = 0
	end
    
	def findAndSub()
		coder = HTMLEntities.new
		line_array = Array.new
		print_line = true

		File.read(@file_path).each_line { 
			|line|
			coder.encode(line, :named)
			line_array.push(line)
		}

		File.open(@file_path, 'w') { |file| file.truncate(0) }

		line_array.each {
			|line|
			string = coder.decode(line)
            
			if (string[/\/\d+\W\d+\/\w+\W\w+/] != nil) && (@counter != @this_replacement_images.getNumImages)
				#puts "Ready to change line..."
				thumb_size_w = 0
				thumb_size_h = 0
				image_name_and_path = @this_replacement_images.getFolderWhereImagesLive().to_s + "/" + @this_replacement_images.nextImage().to_s # build the path to the image file
				string.gsub!(/\/\d+\W\d+\/\w+\W\w+/, "/" + image_name_and_path) # Substitute the path in the file
                
				image_width = Dimensions.width(image_name_and_path)
				string.gsub!(/width=\d+/, "width=" + image_width.to_s) # Substitute the new image's width
                
				image_height = Dimensions.height(image_name_and_path)
				string.gsub!(/height=\d+/, "height=" + image_height.to_s) # Substitute the new image's height
                
				# We'll use the image's height and width to determine the dimensions for the image's thumbnail
				if image_width > image_height
					thumb_size_w = 150
					thumb_size_h = 100
				else
					thumb_size_w = 100
					thumb_size_h = 150
				end

				string.gsub!(/width=\"\d+\"/, "width=\"" + thumb_size_w.to_s + "\"")
				string.gsub!(/height=\"\d+\"/, "height=\"" + thumb_size_h.to_s + "\"")
                
				# Update the image's name
				image_name = @image_prefix + @image_numbering.to_s.rjust(4, "0")
				string.gsub!(/\<p>\w+\<\/p\>/, "<p>" + image_name + "</p>")
				@image_numbering += 1
				@counter += 1
			elsif (string[/\/\d+\W\d+\/\w+\W\w+/] != nil) && @counter == @this_replacement_images.getNumImages
				print_line = false # The string matches our criteria but we've processed all of the new images and don't want to print any more image lines to the html file 
			end
            
			#puts string
            
			if print_line
				open(@file_path, 'a') {
					|the_file|
					the_file.puts string
				}
			elsif !print_line
				print_line = true 
			end
            
		}
        
	end
    
	def getImageNumbering()
		return @counter
	end
    
	def setFilePath(file_path)
		@file_path = file_path
	end
end


# ----------------
# Function: getInput
# Description: Displays a message and checks whether a file or a directory exists
# ----------------
def getInput(message, check_type)
    
	while true
		ok_continue = false
		err_message = ""

		puts message
		value = gets.chomp

		if check_type == "dir"
			if !ok_continue = File.directory?(value)
				err_message = "Sorry - #{value} isn't a valid directory. Please try again..."
			end
		elsif check_type == "file"
			if !ok_continue = File.file?(value)
				err_message = "Sorry - #{value} isn't a valid filename. Please try again..."
			end
		else
			puts "Not specified whether to check for file or directory, so dying gracefully."
			exit
		end

		if ok_continue
			return value # We've been given a valid filename or directory and we're free to keep calm and carry on
		else
			puts err_message
			redo
		end
	end
end

# ***********************************************************************
# Main Program Flow
# ***********************************************************************

replacement_image_directory = getInput("Please give me the path to the directory where your new images are stored: ", "dir")
puts "The new images live here: #{replacement_image_directory}."
#replacement_image_directory = "./08.2014"

update_file_directory = getInput("Sweet! Now, give me the path to the HTML file that needs updated: ", "file")
puts "We'll update the following file: #{update_file_directory}."
#update_file_directory = "./state_vermont.html"

puts "OK - now, what prefix do you want to use for your image's name? (i.e., vt, ut, etc.)"
image_prefix = gets.chomp
puts "Using prefix: #{image_prefix}."
#image_prefix = "vt"

puts "And where should we start numbering your images? (i.e., 1, 10, 100, etc.)"
image_numbering = gets.chomp.to_i
puts "Numbering your images beginning at #{image_numbering}."
#image_numbering = 100

my_pics = ImageManager.new(replacement_image_directory)
puts "Looks  like we have " + my_pics.getNumImages.to_s + " images to process. Here we go!"
my_file_test = FileTextUpdater.new(update_file_directory, my_pics, image_prefix, image_numbering)
my_file_test.findAndSub()

while true
    
	puts "We've processed " + my_file_test.getImageNumbering.to_s + " images so far."
    
	if my_pics.getNumImages == my_file_test.getImageNumbering
		puts "Looks like we're done here!"
		exit
	else
		our_message = "We still have #{my_pics.getNumImages - my_file_test.getImageNumbering} images to process. Please enter a new filename to continue: "
		update_file_directory = getInput(our_message , "file")
		my_file_test.setFilePath(update_file_directory)
		my_file_test.findAndSub()
		redo
	end
end
